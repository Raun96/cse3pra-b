//
//  AuthService.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 9/5/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth

class AuthService {
    
    static let  AuthInstance = AuthService()
    
    //Function to register a user with Firebase and the app.
    func registerUser(withEmail email: String, Password password: String,name: String, userCreationComplete: @escaping (_ status: Bool, _ error : Error?) -> ()) {
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            guard let user = user else  {
                userCreationComplete(false,error)
                return
            }
            print("user created")
            self.addUserToDatabase(withEmail: email, name: name)
            userCreationComplete(true,nil)
        }
    }
    
    //Function to Log In a user.
    func logInUser(withEmail email: String, Password password: String, userLogInComplete: @escaping (_ status: Bool,_ error : Error?) -> ()) {
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            guard let user = user else {
                userLogInComplete(false,error)
                return
            }
            print("User Logged in")
            userLogInComplete(true,nil)
        }
    }
    
    //Function to add user to the Database and create the relevant Documents and Fields.
    func addUserToDatabase(withEmail email: String, name: String){
        let databaseRef = Firestore.firestore().collection("Users")
        let user = Auth.auth().currentUser?.uid
        databaseRef.document(user!).setData(["Email" : email, "Name" :name,"ProfileImage" : "ReferenceToImage"])
        databaseRef.document(user!).collection("Portfolio")
        print("User added to database.......!!!!!")
    }

}
