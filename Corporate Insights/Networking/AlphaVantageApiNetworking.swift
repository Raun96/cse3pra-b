//
//  AlphaVantageApiNetworking.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 9/10/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import Foundation

class AlphaVantageApiNetworking {
    
    let monthlyDictkeys = ["2017-10-31","2017-11-30","2017-12-29","2018-01-31","2018-02-28","2018-03-29","2018-04-30","2018-05-31","2018-06-29","2018-07-31","2018-08-31","2018-09-28"]
    
   
    func getSharePricesForCharts(symbol: String, completed: @escaping (_ highSharePricesArray: [Double], _ lowSharePricesArray :[Double])->()) {
        
        var lowSPrices = [Double]()
        var highSPrices = [Double]()
        let api = "\(alphaVantageBaseApi)TIME_SERIES_MONTHLY&symbol=\(symbol)&apikey=\(alphaVantageApiKey)"
        print(api)
        let apiURL = URL(string: api)
        
        let task = URLSession.shared.dataTask(with: apiURL!) { (data, response, error) in
            guard error == nil else {
                print(error?.localizedDescription)
                return
            }
            guard let data = data else {
                print("No data returned  by api call.")
                return
            }
//            let jsonData = try JSONSerialization.jsonObject(with: data!, options: []) as? [String : Any]
           
            let jsonData = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
            
            self.parseDataForCharts(jsonData: jsonData as! Dictionary<String, AnyObject>, completed: { (high, low) in
                lowSPrices = low
                highSPrices = high
            })
           
            completed(highSPrices,lowSPrices)
        }
        task.resume()
    }
    
    func parseDataForCharts(jsonData :Dictionary<String, AnyObject>, completed: @escaping (_  highSharePricesArray: [Double],_ lowSharePricesArray :[Double]) ->()) {
        var lowSPrices = [Double]()
        var highSPrices = [Double]()

        if let monthlyDict = jsonData["Monthly Time Series"] as? Dictionary<String,AnyObject> {
            
            for i in 0..<monthlyDictkeys.count {
                if let monthlyObject = monthlyDict[monthlyDictkeys[i]] as? Dictionary<String,String> {
                    let highString = monthlyObject["2. high"]
                    let highDouble = NSString(string: highString!).doubleValue
                    highSPrices.append(highDouble)
                    let lowString = monthlyObject["3. low"]
                    let lowDouble = NSString(string: lowString!).doubleValue
                    lowSPrices.append((lowDouble))
                }
            }
        }
        completed(highSPrices,lowSPrices)
        
    }
    
    func getCurrentSharePrices(symbol: String, handler :@escaping (_ highSPrice: String, _ lowSPrice : String)->()) {
        
        var lowSPrices = String()
        var highSPrices = String()
        let api = "\(alphaVantageBaseApi)TIME_SERIES_DAILY&symbol=\(symbol)&apikey=\(alphaVantageApiKey)"
        let apiURL = URL(string: api)
        
        let task = URLSession.shared.dataTask(with: apiURL!) { (data, response, error) in
            guard error == nil else {
                print(error?.localizedDescription)
                return
            }
            guard let data = data else {
                print("No data returned  by api call.")
                return
            }
            
            let jsonData = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
            
            self.parseDataForCurrentSharePrices(jsonData: jsonData as! Dictionary<String, AnyObject>, handler: { (high, low) in
                highSPrices = high
                lowSPrices = low
            })
            handler(highSPrices,lowSPrices)
        }
        task.resume()
    }
    
    func parseDataForCurrentSharePrices(jsonData :Dictionary<String, AnyObject>, handler: @escaping (_ high: String, _ low: String)->()) {
        
        let key = "2018-10-16"
        var highString = String()
        var lowString = String()
        if let dailyDict = jsonData["Time Series (Daily)"] as? Dictionary<String,AnyObject> {
            
            if let dailyObject = dailyDict[key] as? Dictionary<String,String> {
                let high = dailyObject["2. high"]
                let low = dailyObject["3. low"]
                
                 highString = "$" + high!
                 lowString = "$" + low!
            }
        }
        handler(highString, lowString)
    }
}
