
//
//  SearchForCompanies.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 22/5/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import Foundation

class SearchForCompanies {
    var companiesArray = [CompaniesSearched]()
    
    func downloadSearchedCompanies(name: String,downloadComplete : @escaping (_ status : Bool, _ companies: [CompaniesSearched]) -> ())  {

        guard name != "" else {
            print("Company name Can't be nil.")
            return
        }
        
        let coreApi = "\(searchEdgarApi)*\(name)*&limit=10&appkey=\(edgarApiKey)"
        print(coreApi)
    
        let addressURL = URL(string: coreApi)
        let task = URLSession.shared.dataTask(with: addressURL!) { (data, response, error) in
            guard error == nil else {
                print(error?.localizedDescription)
                return
            }
            guard let data = data else {
                print("No data returned  by api call.")
                return
            }
            guard let jsonObj = (try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)) as? Dictionary<String,Any> else {
                print("NO Json Object found.")
                return
            }
        
            let companiesArray = self.parseCompaniesJson(jsonObj: jsonObj)
            downloadComplete(true,companiesArray)
           
        }
        
        task.resume()
        
    }
    
    func parseCompaniesJson(jsonObj : Dictionary<String, Any>) -> [CompaniesSearched] {
        
        if let result = jsonObj["result"] as? Dictionary<String,Any> {
            if let companiesObj = result["rows"] as? [Dictionary<String,Any>] {
                
                for objects in companiesObj {
                    if let values = objects["values"] as? [Dictionary<String,Any>] {
                        var name,cik,primarySymbol,description : String?
                        var entityID : Int = 0
                        for value in values {
                            
                            let field = value["field"] as? String
                            switch field {
                            case "cik" :  let _cik = value["value"] as? String
                            if _cik == nil || _cik == "" {
                                cik = "N/A"
                            } else {
                                cik = _cik!
                                }
                            case "companyname" :  let _name = value["value"] as? String
                            if _name == nil || _name == "" {
                                name = "N/A"
                            } else {
                                name = _name!
                                }
                            case "entityid" :  let _entityID = value["value"] as? Int
                            if _entityID == 0 || _entityID == nil {
                                entityID = 0
                            } else {
                                entityID = _entityID!
                                }
                            case "primarysymbol":  let _primarySymbol = value["value"] as? String
                            if _primarySymbol == "" || _primarySymbol == nil {
                                primarySymbol = "N/A"
                            } else {
                                primarySymbol = _primarySymbol!
                                }
                            case "sicdescription":  let _description = value["value"] as? String
                            if _description == "" || _description == nil {
                                description = "N/A"
                            } else {
                                description = _description!
                                }
                                
                            default : print("None of the fields match")
                            }
                          
                        }
                        if primarySymbol == nil {
                            primarySymbol = "N/A"
                        }
                        if description == nil {
                            description = "N/A"
                        }
                        let searchedCompaniesObj = CompaniesSearched(name: name!, cik: cik!, symbol: primarySymbol!, entity: entityID, description: description!)
                        self.companiesArray.append(searchedCompaniesObj)
                    }
                }
            }
            
        }
        return companiesArray
    }
    
    func downloadCompanyInfo() {
        let xbrlApiUrl = URL(string: xbrlAPI)
        let task = URLSession.shared.dataTask(with: xbrlApiUrl!) { (data, response, error) in
            guard error == nil else {
                print(error!)
                return
            }
            print("This is the data returned from XBRL US.")
            let stringData = String(decoding: data!, as: UTF8.self)
            print(stringData)
        }
        task.resume()
    }
}
