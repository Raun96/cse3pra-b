//
//  XbrlNetworking.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 4/9/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import Foundation
import SwiftyXMLParser

class XbrlNetworking {
    
    func getSearchedCompanyData(cikNo: String,companyDataRetrivedHandler : @escaping (_ status: Bool,_ companyRatios: VisualRatiosModel) -> ()) {
        let year = getYear()
        var visualRatios = VisualRatiosModel(assets: 0, liabilities: 0, revenues: 0, netIncomeLoss: 0, earningsPerShare: 0)
        let elementsToSearch = "assets,liabilities,revenues,netincomeloss,Earnings_Per_Share"
        let xbrlUrl = "\(xbrlBaseAPI)\(elementsToSearch)&Year=\(year)&Period=Y&CIK=\(cikNo)\(xbrlApiKey)"
        print(xbrlUrl)
        let apiUrl = URL(string: xbrlUrl)
        let task = URLSession.shared.dataTask(with: apiUrl!) { (data, response, error) in
            guard error == nil else {
                print(error?.localizedDescription)
                return
            }
            guard let data = data else {
                print("No data returned  by api call.")
                return
            }
            let dataString = String(NSString(data: data, encoding: String.Encoding.utf8.rawValue)!)
            
            let xml = XML.parse(data)
            var numberOfElements = (xml["dataRequest","count",0].int)!
            print(numberOfElements)
            
            while (numberOfElements > 0) {
                numberOfElements -= 1
                let element = xml["dataRequest","fact",numberOfElements,"elementName"].text
                if  element == "Assets" {
                    let assets = xml["dataRequest","fact",numberOfElements,"amount"].double
                    visualRatios.assets = assets!
                } else if element == "Liabilities" {
                    let liabilities = xml["dataRequest","fact",numberOfElements,"amount"].double
                    visualRatios.liabilities = liabilities!
                } else if element == "Revenues" {
                    let revenues = xml["dataRequest","fact",numberOfElements,"amount"].double
                    visualRatios.revenues = revenues!
                } else if element == "NetIncomeLoss" {
                    let netIncomeLoss = xml["dataRequest","fact",numberOfElements,"amount"].double
                    visualRatios.netIncomeLoss = netIncomeLoss!
                }
                else if element == "Earnings_Per_Share" {
                    let eps = xml["dataRequest","fact",numberOfElements,"amount"].double
                    visualRatios.earningsPerShare = eps!
                } else {
                    print("There's an error in the  data returned")
                }
            }
            
            companyDataRetrivedHandler(true,visualRatios)
            
        }
        task.resume()
        
    }
    
    func getNetWorthforChart(cikNo: String, completed: @escaping (_ netWorthArray: [Double], _ yearsArray :[Double]) -> ()) {
        
        let year = getYear()
        let startYear = getStartYear()
        var assetsArray = [Double]()
        var yearArray = [Double]()
        
        let xbrlUrl = "\(xbrlBaseAPI)\("Assets")&StartYear=\(startYear)&Year=\(year)&Period=Y&CIK=\(cikNo)\(xbrlApiKey)"
        print(xbrlUrl)
        let apiUrl = URL(string: xbrlUrl)
        
        let task = URLSession.shared.dataTask(with: apiUrl!) { (data, response, error) in
            guard error == nil else {
                print(error?.localizedDescription)
                return
            }
            guard let data = data else {
                print("No data returned  by api call.")
                return
            }
            let dataString = String(NSString(data: data, encoding: String.Encoding.utf8.rawValue)!)
            
            let xml = XML.parse(data)
            let numberOfElements:Int = (xml["dataRequest","count",0].int)!
            print(numberOfElements)
            
            for i in 0..<numberOfElements {
                let element = xml["dataRequest","fact",i,"elementName"].text
                if  element == "Assets" {
                    let assets = xml["dataRequest","fact",i,"amount"].double
                    assetsArray.append(assets ?? 0)
                    let year = xml["dataRequest","fact",i,"year"].double
                    yearArray.append(year ?? 0)
                }
                
            }
            completed(assetsArray,yearArray)
        }
        task.resume()
        
    }
    
    
    func getYear() -> String {
        let date = Date()
        let calander = Calendar.current
        let year = (calander.component(.year, from: date) - 1)
        return String(year)
    }
    
    func getStartYear() -> String {
        let date = Date()
        let calander = Calendar.current
        let startYear = (calander.component(.year, from: date) - 10)
        return String(startYear)
    }
    
    
}


