//
//  FirebaseNetworking.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 28/8/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import Foundation
import FirebaseFirestore
import Firebase

class FirebaseNetworking {
    private lazy var BASE_REF = Firestore.firestore().collection("Users")
    // to be modified.
    private lazy var userDocument = Auth.auth().currentUser?.uid
    let alphaVantageObj = AlphaVantageApiNetworking()
    func getUserPortfolio(handler: @escaping (_ companies :[PortfolioCompany])->()) {
        var companies = [PortfolioCompany]()
        let databaseREF = BASE_REF.document(userDocument!).collection("Portfolio")
        databaseREF.getDocuments { (dataSnapshot, error) in
            guard error == nil else {
                print("Error while retriving companies.")
                print(error!)
                return
            }
            for document in dataSnapshot!.documents {
                guard let data = document.data() as? Dictionary<String,Any> else {
                    print("Can't retrive data from the objects")
                    return
                }
                let name = data["companyName"] as? String
                let cik = data["CIK"] as? String
                let symbol = data["Symbol"] as? String
                let sharePrice = "00000"
                let netWorth = "000000"
                print(name ,cik, symbol, sharePrice)
                let companyObj = PortfolioCompany(name: name!, cik: cik!, symbol: symbol!, sharePrice: sharePrice, netWorth: "1000000")
                companies.append(companyObj)
            }
            handler(companies)
        }
        
    }
    
    func queryPortfolioForCompany(cik: String, handler :@escaping (_ status: Bool)->()) {
        let docRef = BASE_REF.document(userDocument!).collection("Portfolio").document(cik)
        print(cik)
        var isDocumentPresent = Bool()
        docRef.getDocument { (document, error) in
            isDocumentPresent = (document?.exists)!
            print(document?.exists)
            handler(isDocumentPresent)
        }
    }
    
    func addCompanyToPortfolio(company: CompaniesSearched, handler :@escaping (_ status: Bool) -> ()) {
        let docRef = BASE_REF.document(userDocument!).collection("Portfolio").document(company.cikNo)
        docRef.setData(["CIK": company.cikNo, "Symbol": company.primarySymbol, "companyName" : company.companyName, "Description" : company.companyDescription, "HighSharePrices" : "$111.1", "LowSharePrices" : "$000.0", "DateSPUpdated" : "1/1/1"]) { (err) in}
        handler(true)
    }
    
    func getTodaySharePrices(handler :@escaping (_ todaySharePrices :[TodaySharePrice]) -> ()) {
        var companiesSharePrices = [TodaySharePrice]()
        let databaseREF = BASE_REF.document(userDocument!).collection("Portfolio")
        databaseREF.getDocuments { (dataSnapshot, error) in
            guard error == nil else {
                print("Error while retriving companies.")
                print(error!)
                return
            }
            for document in dataSnapshot!.documents {
                guard let data = document.data() as? Dictionary<String,Any> else {
                    print("Can't retrive data from the objects")
                    return
                }
                let symbol = data["Symbol"] as? String
                var highSharePrice = String()
                var lowSharePrice = String()
                let currentDate = self.getCurrentDate()
                let dateSharePricesLastUpdated = data["DateSPUpdated"] as? String
                
                if currentDate != dateSharePricesLastUpdated {
                    self.updateSharePrices(documentId: document.documentID, handler: { (high, low) in
                        highSharePrice = high
                        lowSharePrice = low
                    })
                } else {
                    highSharePrice = (data["HighSharePrices"] as? String)!
                    lowSharePrice = (data["LowSharePrices"] as? String)!
                }
                let company = TodaySharePrice(symbol: symbol!, high: highSharePrice, low: lowSharePrice)
                companiesSharePrices.append(company)
                
            }
            handler(companiesSharePrices)
        }
    }
    
    func updateSharePrices(documentId : String, handler :@escaping (_ high: String, _ low :String)->()){
        var companySymbol = ""
        let documentRef = BASE_REF.document(userDocument!).collection("Portfolio").document(documentId)
        documentRef.getDocument { (documentSnapshot, error) in
            if let data = documentSnapshot?.data()  {
                companySymbol = (data["Symbol"] as? String)!
            }
            self.alphaVantageObj.getCurrentSharePrices(symbol: companySymbol, handler: { (high, low) in
                documentRef.updateData(["HighSharePrices" : high, "LowSharePrices" : low, "DateSPUpdated": self.getCurrentDate() ])
                handler(high,low)
            })
        }
        
    }
    
    func getCurrentDate() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.none
        dateFormatter.dateStyle = DateFormatter.Style.short
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
}
