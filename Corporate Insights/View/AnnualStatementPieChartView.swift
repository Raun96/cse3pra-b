//
//  AnnualStatementPieChartView.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 30/5/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import Charts

class AnnualStatementPieChartView: PieChartView {

    func updateAssetLiabalitiesChartData(chartRef: PieChartView, assets: Double, liabalities: Double) {
        
        let assetsData = PieChartDataEntry(value: assets)
        let liabilitiesData = PieChartDataEntry(value: liabalities)
        let piechartDataEntries: [PieChartDataEntry] = [assetsData,liabilitiesData]
        
        assetsData.label = "Assets"
        liabilitiesData.label = "Liabalities"
        let pchartDataSet = PieChartDataSet(values: piechartDataEntries, label: nil)
        let chartDataObj = PieChartData(dataSet: pchartDataSet)
        chartRef.chartDescription?.text = ""
        chartRef.backgroundColor = UIColor(red: 220/255.0, green: 220/255.0, blue: 220/255.0, alpha: 1.0)
        chartRef.holeColor = UIColor(red: 220/255.0, green: 220/255.0, blue: 220/255.0, alpha: 1.0)
        
//        let greenColor = UIColor(red: 152.0/255.0, green: 251.0/255.0, blue: 152.0/255.0, alpha: 1.0)
//        let redColor = UIColor(red: 242.0/255.0, green: 73.0/255.0, blue: 16.0/255.0, alpha: 1.0)
        let chartColors = [UIColor(red: 133/255.0, green: 187/255.0, blue: 101/255.0, alpha: 1.0),UIColor(red: 242.0/255.0, green: 73.0/255.0, blue: 16.0/255.0, alpha: 1.0)]
        
 
        pchartDataSet.colors = chartColors 
        chartRef.data = chartDataObj
        
    }
   

}
