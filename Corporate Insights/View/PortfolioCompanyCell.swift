//
//  PortfolioCompanyCell.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 28/8/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import UIKit

class PortfolioCompanyCell: UITableViewCell {

    @IBOutlet weak var symbolLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var companyTypeLbl: UILabel!
    func updateCell(symbol: String, name: String, type: String) {
        symbolLbl.text = symbol
        nameLbl.text = name
        companyTypeLbl.text = type
    }

}
