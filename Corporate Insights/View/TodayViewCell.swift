//
//  TodayViewCell.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 16/10/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import UIKit

class TodayViewCell: UITableViewCell {

    @IBOutlet weak var companySymbolLbl: UILabel!
    @IBOutlet weak var lowSharePriceLbl: UILabel!
    @IBOutlet weak var highSharePriceLbl: UILabel!
    
    func updateCell(companySymbol: String, high: String, low : String) {
        companySymbolLbl.text = companySymbol
        lowSharePriceLbl.text = low
        highSharePriceLbl.text = high
    }

}
