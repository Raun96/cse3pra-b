//
//  sharepricesCell.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 3/6/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import UIKit

class sharepricesCell: UITableViewCell {

    @IBOutlet weak var companyNameLbl: UILabel!
    @IBOutlet weak var sharepriceLbl: UILabel!
    
    func updateCell(shareObj: TodaySharePrice) {
        companyNameLbl.text = shareObj.companyName
        if shareObj.color == "red" {
            sharepriceLbl.text = shareObj.sharePrice
            sharepriceLbl.textColor = UIColor(red: 242.0/255.0, green: 73.0/255.0, blue: 16.0/255.0, alpha: 1.0)
        } else {
            sharepriceLbl.text = shareObj.sharePrice
            sharepriceLbl.textColor = UIColor(red: 152.0/255.0, green: 251.0/255.0, blue: 152.0/255.0, alpha: 1.0)
        }
    }
}
