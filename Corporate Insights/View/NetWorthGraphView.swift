//
//  NetWorthGraphView.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 8/10/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import UIKit
import Charts


class NetWorthGraphView: UIView {

    @IBOutlet weak var netWorthChart: BarChartView!
    
    //Upadate It with data entries....!!
    func setUpChartdata(dataPoints: [Double], values: [Double], message: String) {
        netWorthChart.drawGridBackgroundEnabled = true
        var dataEntries: [BarChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(x: dataPoints[i], y: values[i])
            dataEntries.append(dataEntry)
        }
        let labelMessage = "Net Worth in \(message)"
        let chartDataSet = BarChartDataSet(values: dataEntries, label: labelMessage)
        let chartData = BarChartData(dataSet: chartDataSet)
        
        netWorthChart.data = chartData
        netWorthChart.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInBack)
        netWorthChart.chartDescription?.text = ""
        
//        netWorthChart.leftAxis.enabled = false
//        netWorthChart.rightAxis.enabled = false
        netWorthChart.rightAxis.enabled = false
//        netWorthChart.xAxis.enabled = false
        netWorthChart.leftAxis.drawGridLinesEnabled = false
        netWorthChart.xAxis.drawGridLinesEnabled = false
        
    }
    
    
    
    
}
