//
//  searchedCompaniesCell.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 23/5/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import UIKit

class searchedCompanyCell: UITableViewCell {

   
    @IBOutlet weak var primarySymbolLbl: UILabel!
    @IBOutlet weak var companyNameLbl: UILabel!
    @IBOutlet weak var companyDescriptionLbl: UILabel!
    
    
    //Updating the Searched Company Cell from the parsed data.
    func updateSearchedCompanyCell(name: String, symbol: String, description: String) {
 
        primarySymbolLbl.text = symbol
        companyNameLbl.text = name
        companyDescriptionLbl.text = description
    }
    

}
