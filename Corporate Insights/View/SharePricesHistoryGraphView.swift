//
//  SharePricesHistoryGraphView.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 9/10/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import UIKit
import Charts

class SharePricesHistoryGraphView: UIView {


    @IBOutlet weak var lineChartView: LineChartView!
    
    func setUpChartData(dataPoints: [Double], values: [Double]) {
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(x: dataPoints[i], y: values[i])
            dataEntries.append(dataEntry)
        }
        
        let lineChartDataSet = LineChartDataSet(values: dataEntries, label: "Share Prices in $.")
        let lineChartData = LineChartData(dataSet: lineChartDataSet)
        lineChartView.data = lineChartData
        
    }
    
}
