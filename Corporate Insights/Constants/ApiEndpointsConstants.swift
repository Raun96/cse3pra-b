//
//  ApiEndpointsConstants.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 22/5/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import Foundation

//Edgar's API for searching a company by its company name.
let searchEdgarApi = "http://datafied.api.edgar-online.com/v2/companies.json?companynames="
let edgarApiKey = "5w89zrecuzdr63wau2kabksb"
//*micro*&limit=10&appkey={APPKEY}

let xbrlAPI = "http://csuite.xbrl.us/php/dispatch.php?Task=xbrlValues&Element=Assets&Year=2014&Period=Y&CIK=0000732717&API_Key=ba7ec060-636b-4325-b0e4-022ad6210512&DimReqd=False"

let xbrlBaseAPI = "http://csuite.xbrl.us/php/dispatch.php?Task=xbrlValues&Element="
let xbrlApiKey = "&API_Key=ba7ec060-636b-4325-b0e4-022ad6210512&DimReqd=False"

let alphaVantageApiKey = "ESS1IJ90K9KVLKJP"

//https://www.alphavantage.co/query?function=TIME_SERIES_MONTHLY&symbol=MSFT&apikey=demo

let alphaVantageBaseApi = "https://www.alphavantage.co/query?function="
