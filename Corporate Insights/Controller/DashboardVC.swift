//
//  DashboardVC.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 30/5/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import UIKit
import Charts

class DashboardVC: UIViewController {

    var demoCompaniesArray = [AssetsLiabilities]()
    var todaySharePricesArray = [TodaySharePrice]()
    var firebaseObj = FirebaseNetworking()
   
    @IBOutlet weak var sharePricesTableView: UITableView!
    @IBOutlet weak var selectedCompanyNameLbl: UILabel!
    @IBOutlet weak var companyPickerView: UIPickerView!
    @IBOutlet weak var assetsLiabilitiesChart: PieChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        companyPickerView.delegate = self
        companyPickerView.dataSource = self
        sharePricesTableView.delegate = self
        sharePricesTableView.dataSource = self
        createPickerViewDemoData()
        assetsLiabilitiesChart.rotationWithTwoFingers = true
        firebaseObj.getTodaySharePrices { (companiesSharePrices) in
            self.todaySharePricesArray = companiesSharePrices
            print(self.todaySharePricesArray.count)
            self.sharePricesTableView.reloadData()
        }

    }
    
    @IBAction func selectCompanyBtnPressed(_ sender: Any) {
        companyPickerView.isHidden = false
    }
    
}

//extension for the company picker view // to be update later.
extension DashboardVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
        
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return demoCompaniesArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return demoCompaniesArray[row].companyName
       
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let chart = AnnualStatementPieChartView()
        chart.updateAssetLiabalitiesChartData(chartRef: self.assetsLiabilitiesChart, assets: demoCompaniesArray[row].assets, liabalities: demoCompaniesArray[row].liabalities)
        selectedCompanyNameLbl.text = demoCompaniesArray[row].companyName
        pickerView.isHidden = true
    }
    
    func createPickerViewDemoData() {
        let c1 = AssetsLiabilities(assets: 28655372, liabalities: 24418130, companyName: "Tesla Inc.")
        demoCompaniesArray.append(c1)
        let c2 = AssetsLiabilities(assets: 212482000, liabalities: 177481000, companyName: "general Motors")
        demoCompaniesArray.append(c2)
        let c3 = AssetsLiabilities(assets: 1057161, liabalities: 254340, companyName: "Steve Madden")
        demoCompaniesArray.append(c3)
    }

}

//extension for creating demo share prices // to be updated later
extension DashboardVC:  UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(todaySharePricesArray.count)
        return todaySharePricesArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = sharePricesTableView.dequeueReusableCell(withIdentifier: "sharepricesCell") as? TodayViewCell {
            let companyObj = todaySharePricesArray[indexPath.row]
            cell.updateCell(companySymbol: companyObj.companySymbol, high: companyObj.highSharePrice, low: companyObj.lowSharePrice)
            return cell
        } else {
            return TodayViewCell()

        }
    }
 
}
