//
//  DisplayCompanyInfoVC.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 7/8/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import UIKit
import SwiftyXMLParser

class DisplayCompanyInfoVC: UIViewController {
    
    var company = CompaniesSearched(name: "", cik: "", symbol: "", entity: 0, description: "")
    var companyVisualRatios = VisualRatiosModel(assets: 0, liabilities: 0, revenues: 0, netIncomeLoss: 0, earningsPerShare: 0)
    let firebaseObj = FirebaseNetworking()
    var isPresentInDatabase = Bool()

    
    @IBOutlet weak var companyNameLbl: UILabel!
    @IBOutlet weak var sharePriceLbl: UILabel!
    @IBOutlet weak var netWorthLbl: UILabel!
    @IBOutlet weak var assetsLbl: UILabel!
    @IBOutlet weak var addButton: UIBarButtonItem!
    @IBOutlet weak var liabilitiesLbl: UILabel!
    @IBOutlet weak var epsLbl: UILabel!
    @IBOutlet weak var netIncomeLossLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(company.cikNo)
        getSearchedCompanyData(cikNo: company.cikNo)
//        convertToDollars(data: 5372286218898)
        firebaseObj.queryPortfolioForCompany(cik: company.cikNo) { (status) in
            self.isPresentInDatabase = status
            print("This is the status of the company ----- \(self.isPresentInDatabase)")
            if self.isPresentInDatabase{
                self.addButton.title = "Added"
            }

        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        print("View did appear....")
    }
    @IBAction func backButonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addButtonPressed(_ sender: Any) {
        if addButton.title != "Added" {
            firebaseObj.addCompanyToPortfolio(company: company) { (status) in
                if status {
                    self.addButton.title = "Added"
                }
            }
        }
    }
    
    func updateRatios() {
        
    }
    
    func convertToDollars(data: Double){
        var stringData = String(data)
        while stringData.count > 0 {
            if (stringData.count/3) >= 1 {
                let index = stringData.index(stringData.endIndex, offsetBy: -3)
//                var substring = stringData[index...]
//                print(substring)
                stringData = String(stringData[...index])
            }
        }
        print(stringData)
    }
    
    func getSearchedCompanyData(cikNo: String) {
        let year = getYear()
        var visualRatios = VisualRatiosModel(assets: 0, liabilities: 0, revenues: 0, netIncomeLoss: 0, earningsPerShare: 0)
        let elementsToSearch = "assets,liabilities,revenues,netincomeloss,Earnings_Per_Share"
        let xbrlUrl = "\(xbrlBaseAPI)\(elementsToSearch)&Year=\(year)&Period=Y&CIK=\(cikNo)\(xbrlApiKey)"
        print(xbrlUrl)
        let apiUrl = URL(string: xbrlUrl)
        let task = URLSession.shared.dataTask(with: apiUrl!) { (data, response, error) in
            guard error == nil else {
                print(error?.localizedDescription)
                return
            }
            guard let data = data else {
                print("No data returned  by api call.")
                return
            }
            print("Printing Data........................")
            let dataString = String(NSString(data: data, encoding: String.Encoding.utf8.rawValue)!)
            print(dataString)
            let xml = XML.parse(data)
            var numberOfElements = (xml["dataRequest","count",0].int)!
            print(numberOfElements)
            
            while (numberOfElements > 0) {
                numberOfElements -= 1
                let element = xml["dataRequest","fact",numberOfElements,"elementName"].text
                if  element == "Assets" {
                    let assets = xml["dataRequest","fact",numberOfElements,"amount"].double
                    visualRatios.assets = assets!
                } else if element == "Liabilities" {
                    let liabilities = xml["dataRequest","fact",numberOfElements,"amount"].double
                    visualRatios.liabilities = liabilities!
                } else if element == "Revenues" {
                    let revenues = xml["dataRequest","fact",numberOfElements,"amount"].double
                    visualRatios.revenues = revenues!
                } else if element == "NetIncomeLoss" {
                    let netIncomeLoss = xml["dataRequest","fact",numberOfElements,"amount"].double
                    visualRatios.netIncomeLoss = netIncomeLoss!
                }
                else if element == "Earnings_Per_Share" {
                    let eps = xml["dataRequest","fact",numberOfElements,"amount"].double
                    visualRatios.earningsPerShare = eps!
                } else {
                    print("There's an error in the  data returned")
                }
            }
            print(visualRatios)
            
//            self.companyVisualRatios = visualRatios
            
        }
        task.resume()
        
    }
    
    
    func getYear() -> String {
        let date = Date()
        let calander = Calendar.current
        
        let year = (calander.component(.year, from: date) - 1)
        return String(year)
    }
}
