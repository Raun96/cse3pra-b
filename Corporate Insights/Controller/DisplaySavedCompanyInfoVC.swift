//
//  DisplaySavedCompanyInfoVC.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 7/10/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import UIKit

class DisplaySavedCompanyInfoVC: UIViewController {
    
    
    @IBOutlet weak var companyNameLbl: UILabel!
    @IBOutlet weak var netWorthLbl: UILabel!
    @IBOutlet weak var totalAssetsLbl: UILabel!
    @IBOutlet weak var totalLiabilitiesLbl: UILabel!
    @IBOutlet weak var epsLbl: UILabel!
    @IBOutlet weak var netIncomeLossLbl: UILabel!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var ScrollView: UIScrollView!
    var company = PortfolioCompany(name: "", cik: "", symbol: "", sharePrice: "", netWorth: "")
    
//    var yearsArray: [Double] = [2010,2011,2012,2013,2014,2015,2016,2017,2018]
//    var netWorthArray : [Double] = [220.0, 190.4, 100.8, 250.2, 180.9, 140.6, 201.6, 96.4, 153.4]
    var netWorthArray = [Double]()
    var yearsArray = [Double]()
    var highSharePricesArray = [Double]()
    var lowSharePricesArray = [Double]()
    let lastmonthsArray: [Double] = [1,2,3,4,5,6,7,8,9,10,11,12]
    let xbrlNetworkingObj = XbrlNetworking()
    let alphaVantageNetworkingObj = AlphaVantageApiNetworking()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        activityIndicator.startAnimating()
        let scrollViewWidth = calculateScrollViewSize()
        let scrollViewHeight = Double(ScrollView.frame.height)
        ScrollView.contentSize = CGSize(width: scrollViewWidth, height: scrollViewHeight)
        ScrollView.isPagingEnabled = true
        
        print(company.cik)
        
        xbrlNetworkingObj.getSearchedCompanyData(cikNo: company.cik) { (status, companyInfo) in
            if status {
                DispatchQueue.main.async {
                    self.updateUI(companyInfo: companyInfo)

                }
            }
        }
        
        xbrlNetworkingObj.getNetWorthforChart(cikNo: company.cik) { (returnedAssetsArray, returnedYearArray) in
            self.netWorthArray = returnedAssetsArray
            self.yearsArray = returnedYearArray
            print("Count of returned demo Array \(self.netWorthArray.count)")
            print(self.netWorthArray)
            DispatchQueue.main.async {
                let message = self.checkAndUpdateChartData(dataArray: self.netWorthArray)
                self.loadNetWorthChart(message: message)
            }
        }
        
        print("Printing share prices")
        alphaVantageNetworkingObj.getSharePricesForCharts(symbol: company.symbol) { (high, low) in
            self.highSharePricesArray = high
            self.lowSharePricesArray = low
            print(self.highSharePricesArray)
            DispatchQueue.main.async {
                self.loadSharePricesChart()
               
            }
        }
    }
    
    @IBAction func exitButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}


extension DisplaySavedCompanyInfoVC {
    
    func calculateScrollViewSize() -> Double {
        
        let screenWidth = UIScreen.main.bounds.width
        let scrollviewWidth = Double(screenWidth * 3.0)
        
        return scrollviewWidth
    }
    
    func loadNetWorthChart(message : String) {
        if let netWorthChartView = Bundle.main.loadNibNamed("NetWorthHistoryView", owner: self, options: nil)?.first as? NetWorthGraphView {
            netWorthChartView.setUpChartdata(dataPoints: yearsArray, values: netWorthArray, message: message)
            ScrollView.addSubview(netWorthChartView)
            netWorthChartView.frame.size.width = UIScreen.main.bounds.width - 40.0
            netWorthChartView.frame.size.height =  ScrollView.frame.size.height
            
        }
    }
    
    func loadSharePricesChart() {
        if let sharePricesChartView = Bundle.main.loadNibNamed("SharePriceshistoryView", owner: self, options: nil)?.first as? SharePricesHistoryGraphView {
            sharePricesChartView.setUpChartData(dataPoints: lastmonthsArray, values: highSharePricesArray)
            ScrollView.addSubview(sharePricesChartView)
            sharePricesChartView.frame = CGRect(x: (UIScreen.main.bounds.width - 40.0), y: 0, width: UIScreen.main.bounds.width - 40.0, height: ScrollView.frame.size.height)
        }
    }
    
    func checkAndUpdateChartData(dataArray :[Double]) -> String {
        let tempArray = dataArray
        var stringToReturn = ""
        if (tempArray[0]/1000000000) > 1 {
            for i in 0..<tempArray.count {
                let data = tempArray[i]/1000000000
                self.netWorthArray[i] = data
            }
            stringToReturn = "Billions of $."
        } else if (tempArray[0]/1000000) > 1 {
            for i in 0..<tempArray.count {
                let data = tempArray[i]/1000000
                self.netWorthArray[i] = data
            }
            stringToReturn = "Millions of $."
        }
        return stringToReturn
    }
    
    func updateUI(companyInfo:VisualRatiosModel) {
        companyNameLbl.text = company.name
        netWorthLbl.text = String(companyInfo.revenues)
        epsLbl.text = String(companyInfo.earningsPerShare)
        totalLiabilitiesLbl.text = String(companyInfo.liabilities)
        totalAssetsLbl.text = String(companyInfo.assets)
        netIncomeLossLbl.text = String(companyInfo.netIncomeLoss)
        activityIndicator.stopAnimating()
    }
}
