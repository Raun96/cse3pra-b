//
//  ProfileVC.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 28/5/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import UIKit
import Firebase

class ProfileVC: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

  
    @IBAction func logOutBtnPressed(_ sender: Any) {
        do{
            try Auth.auth().signOut()
            let loginVC = storyboard?.instantiateViewController(withIdentifier: "loginVC") as? LoginVC
            self.present(loginVC!, animated: true, completion: nil)
        } catch {
            print("Error signing Out \(error.localizedDescription)")
            Alert.showBasic(title: "User can't Log Out.", message: "", vc: self)
        }
    }
    
}
