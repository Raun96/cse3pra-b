//
//  PortfolioVC.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 28/8/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import UIKit

class PortfolioVC: UIViewController {
    
    let firebaseObj = FirebaseNetworking()
    @IBOutlet weak var portfolioTableView: UITableView!
    @IBOutlet weak var emptyLbl: UILabel!
    var companies = [PortfolioCompany]()
    override func viewDidLoad() {
        super.viewDidLoad()
        portfolioTableView.delegate = self 
        portfolioTableView.dataSource = self
        
        firebaseObj.getUserPortfolio { (returnedCompanies) in
            self.companies = returnedCompanies
            print(self.companies.count)
            self.portfolioTableView.reloadData()
        }
    }



}

extension PortfolioVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if companies.count > 0 {
            emptyLbl.isHidden = true
        } else {
            emptyLbl.isHidden = false
        }
        return companies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = portfolioTableView.dequeueReusableCell(withIdentifier: "PortfolioCompanyCell") as? PortfolioCompanyCell else {
            return UITableViewCell()
        }
        let obj = companies[indexPath.row]
        print(obj.cik)
        cell.updateCell(symbol: obj.symbol, name: obj.name, type: "Industry Type")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//                let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//                let companyInfoVC = storyboard.instantiateViewController(withIdentifier: "DisplaySavedCompanyInfoVC")
//                self.present(companyInfoVC, animated: true, completion: nil)
         performSegue(withIdentifier: "displaySavedCompanyInfoVC", sender: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "displaySavedCompanyInfoVC" {
            let controller = segue.destination as! DisplaySavedCompanyInfoVC
            let selectedCompany = companies[(self.portfolioTableView.indexPathForSelectedRow?.row)!]
            print(selectedCompany)
            controller.company.cik = selectedCompany.cik
            controller.company.name = selectedCompany.name
            controller.company.netWorth = selectedCompany.netWorth
            controller.company.symbol = selectedCompany.symbol
        }
        
    }

}
