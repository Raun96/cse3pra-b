//
//  SignUpVC.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 8/5/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import UIKit

class SignUpVC: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var verifypasswordTF: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
    
    }

}

//extension for signUp Button Pressed.
extension SignUpVC {
    @IBAction func signUpBtnPressed(_ sender: Any) {
        
        guard nameTF.text != "" else {
            let alertText = "Name can't be left empty."
            let alertMessage = ""
            Alert.showBasic(title: alertText, message: alertMessage , vc: self)
            print("Name TF is empty.")       //errors to be handled i.e Alerts to be shown.
            return
        }
        guard emailTF.text != "" else {
            let alertText = "Email can't be left empty."
            let alertMessage = ""
            Alert.showBasic(title: alertText, message: alertMessage , vc: self)
            print("email TF is empty.")     //errors to be handled.
            return
        }
        guard passwordTF.text != "" && verifypasswordTF.text != "" else {
            let alertText = "Password can't be left empty."
            let alertMessage = ""
            Alert.showBasic(title: alertText, message: alertMessage , vc: self)
            print("passwords TF is empty")
            return
        }
        if passwordTF.text != verifypasswordTF.text {
            let alertText = "Passwords don't match."
            let alertMessage = "Re-enter passwords."
            Alert.showBasic(title: alertText, message: alertMessage , vc: self)
            print("passwords don't match.")     //errors to be handled. Alerts to be shown
            return
        } else {
            AuthService.AuthInstance.registerUser(withEmail: emailTF.text!, Password: passwordTF.text!, name: nameTF.text!) { (status, registerationError) in
                if status {
                    AuthService.AuthInstance.logInUser(withEmail: self.emailTF.text!, Password: self.passwordTF.text!, userLogInComplete: { (logInStatus, error) in
                        guard logInStatus else {
                            print("Error \(error?.localizedDescription)")
                            //handle the error by popping an alert.
                            return
                        }
                        let tabBar = self.storyboard?.instantiateViewController(withIdentifier: "tabBarVC") as? TabBarVC
                        self.present(tabBar!, animated: true, completion: nil)
                    })
                } else {
                    print("Error \(registerationError?.localizedDescription)")
                    //handle the error.
                }
            }
        }
        
    }
}

// extension for keyboard. Still to improve upon.
extension SignUpVC {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == passwordTF || textField == verifypasswordTF {
            UIView.animate(withDuration: 0.25) {
                self.view.frame.origin.y = -200.0
            }
            
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.25) {
            self.view.frame.origin.y = 0.0
        }
        
    }
    

}
