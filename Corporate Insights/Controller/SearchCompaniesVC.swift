//
//  SearchCompaniesVC.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 21/5/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import UIKit

class SearchCompaniesVC: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var companiesArray: [CompaniesSearched] = []
    let searchForCompaniesObj = SearchForCompanies()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        activityIndicator.isHidden = true
        searchForCompaniesObj.downloadCompanyInfo()
    }

    @IBAction func backBtnPressed(_ sender: Any) {
        self.view.endEditing(true)
        dismiss(animated: true, completion: nil)
    }
    

}

//extension for searchBar. to be updated so that an alert pops up if the name is less than 4 letters.

extension SearchCompaniesVC {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        let companyToSearch = searchBar.text
        guard companyToSearch != "" else {
            Alert.showBasic(title: "Search Bar can't be left blank.", message: "", vc: self)
            return
        }
        if (companyToSearch?.count)! < 3 {
            Alert.showBasic(title: "Searched Company too short.", message: "Enter more than 3 characters in the search bar.", vc: self)
            return
        }
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        
        searchForCompaniesObj.downloadSearchedCompanies(name: companyToSearch!) { (status,returnedCompaniesArray) in
            if status {
                print("returned companies Array count = \(returnedCompaniesArray.count)")
                self.companiesArray = returnedCompaniesArray
                
            }
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                self.activityIndicator.hidesWhenStopped = true
                self.tableView.reloadData()
            }
        }
        
        self.view.endEditing(true)
        
    }
    
}

// Extension for Updating the tableView.
extension SearchCompaniesVC {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         print("Row length \(companiesArray.count)")
        return companiesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "searchedCompanyCell") as? searchedCompanyCell {
            let company = companiesArray[indexPath.row]
            print(company)
            cell.updateSearchedCompanyCell(name: company.companyName, symbol: company.primarySymbol, description: company.companyDescription)
            return cell
        } else{
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(companiesArray[indexPath.row])
//        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//        let infoVc = storyboard.instantiateViewController(withIdentifier: "showinfoVC")
//        self.present(infoVc, animated: true, completion: nil)
        performSegue(withIdentifier: "displayCompanyInfoVC", sender: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "displayCompanyInfoVC" {
            let controller = segue.destination as! DisplayCompanyInfoVC
            let selectedCompany = companiesArray[(self.tableView.indexPathForSelectedRow?.row)!]
            controller.company.companyName = selectedCompany.companyName
            controller.company.cikNo = selectedCompany.cikNo
            controller.company.primarySymbol = selectedCompany.primarySymbol
        }
    }
    
}









