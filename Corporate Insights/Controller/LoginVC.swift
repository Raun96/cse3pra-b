//
//  ViewController.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 6/5/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
     
    }



    @IBAction func signUpBtnPressed(_ sender: Any) {
        let signUpVC = storyboard?.instantiateViewController(withIdentifier: "signUpVC") as? SignUpVC
        present(signUpVC!, animated: true, completion: nil)
        }
    
}

extension LoginVC {
    
    @IBAction func signInBtnPressed(_ sender: Any) {
        guard emailTF.text != "" else {
            print("EmailTF is empty........")       //alerts to be shown. i.e errors to be handled.
            return
        }
        guard passwordTF.text != "" else {
            print("passwordTF is empty......")      //alerts to be shown. i.e errors to be handled.
            return
        }
        
        AuthService.AuthInstance.logInUser(withEmail: emailTF.text!, Password: passwordTF.text!) { (logInStatus, error) in
            if logInStatus {
                let tabBarVC = self.storyboard?.instantiateViewController(withIdentifier: "tabBarVC")
                self.present(tabBarVC!, animated: true, completion: nil)
            } else {
                
                print(error)
                let title = "User is not Signed Up"
                let message = "Sign Up with Corporate Insights to log In."
                Alert.showBasic(title: title, message: message, vc: self)
                //handle the login error here.
            }
        }
    }
}
