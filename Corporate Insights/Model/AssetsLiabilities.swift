//
//  AssetsLiabilities.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 30/5/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import Foundation

struct AssetsLiabilities {
    var assets : Double
    var liabalities: Double
    var companyName : String
}
