//
//  TodaySharePrice.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 3/6/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import Foundation

struct TodaySharePrice {
    var companySymbol: String
    var highSharePrice : String
    var lowSharePrice : String

    init(symbol: String, high: String, low : String) {
        self.companySymbol = symbol
        self.highSharePrice = high
        self.lowSharePrice = low
    }
}

