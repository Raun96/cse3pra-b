//
//  VisualRatiosModel.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 4/9/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import Foundation

struct VisualRatiosModel {
    var assets: Double
    var liabilities: Double
    var revenues: Double
    var netIncomeLoss: Double
    var earningsPerShare: Double
}
