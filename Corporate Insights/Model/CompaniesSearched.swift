//
//  SearchedCompanies.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 22/5/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import Foundation

struct CompaniesSearched {
    var companyName: String
    var cikNo : String
    var entityID : Int
    var primarySymbol : String
    var companyDescription : String
    
    init(name: String, cik: String, symbol: String, entity: Int, description: String) {
        self.companyName = name
        self.cikNo = cik
        self.primarySymbol = symbol
        self.entityID = entity
        self.companyDescription = description
    }
}
