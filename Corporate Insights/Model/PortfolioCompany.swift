
//
//  portfolioCompany.swift
//  Corporate Insights
//
//  Created by Raunak Singh on 28/8/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import Foundation

struct PortfolioCompany {
    var name : String
    var cik: String
    var symbol: String
    var sharePrice : String
    var netWorth : String
    
    init(name: String, cik: String, symbol: String, sharePrice: String, netWorth: String) {
        self.name = name
        self.cik = cik
        self.symbol = symbol
        self.sharePrice = sharePrice
        self.netWorth = netWorth
    }
}
